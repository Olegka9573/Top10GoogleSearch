package com.olegka.googlesearch.presentation.main;


import com.arellomobile.mvp.InjectViewState;
import com.olegka.googlesearch.data.repository.SearchResultsRepository;
import com.olegka.googlesearch.domain.SearchResult;
import com.olegka.googlesearch.presentation.progress.ProgressPresenter;

import java.util.List;

@InjectViewState
public class MainPresenter extends ProgressPresenter<MainView> {

    private SearchResultsRepository searchResultsRepository = new SearchResultsRepository();

    public void getSearchResults(String searchRequest) {
        if (!searchRequest.isEmpty()) {
            showProgress(true);
            searchResultsRepository.getTop10SearchResults(searchRequest)
                    .subscribe(this::handleResults,
                            throwable -> {
                                showProgress(false);
                                handleError(throwable);
                            });
        }
    }

    private void handleResults(List<SearchResult> searchResults) {
        showProgress(false);
        if (!searchResults.isEmpty()) {
            getViewState().onSearchResultsReceived(searchResults);
        } else {
            getViewState().onEmptyResults();
        }
    }
}
