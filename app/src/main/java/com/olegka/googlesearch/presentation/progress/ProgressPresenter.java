package com.olegka.googlesearch.presentation.progress;


import com.arellomobile.mvp.MvpPresenter;

public abstract class ProgressPresenter<View extends ProgressView> extends MvpPresenter<View> {
    protected void showProgress(boolean showProgress) {
        getViewState().showProgress(showProgress);
    }
    protected void handleError(Throwable throwable) {
        getViewState().onError(throwable);
    }
}
