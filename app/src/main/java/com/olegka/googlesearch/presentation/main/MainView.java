package com.olegka.googlesearch.presentation.main;


import com.arellomobile.mvp.viewstate.strategy.SingleStateStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.olegka.googlesearch.domain.SearchResult;
import com.olegka.googlesearch.presentation.progress.ProgressView;

import java.util.List;

public interface MainView extends ProgressView {
    @StateStrategyType(SingleStateStrategy.class)
    void onSearchResultsReceived(List<SearchResult> searchResults);

    @StateStrategyType(SingleStateStrategy.class)
    void onEmptyResults();
}
