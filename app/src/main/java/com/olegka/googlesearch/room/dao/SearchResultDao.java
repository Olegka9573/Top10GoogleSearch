package com.olegka.googlesearch.room.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.olegka.googlesearch.domain.SearchResult;

import java.util.List;

import io.reactivex.Flowable;

@Dao
public interface SearchResultDao {
    @Query("SELECT * FROM searchResult")
    Flowable<List<SearchResult>> getAll();

    @Insert
    void insert(List<SearchResult> employees);

    @Query("DELETE FROM searchResult")
    void deleteAll();
}
