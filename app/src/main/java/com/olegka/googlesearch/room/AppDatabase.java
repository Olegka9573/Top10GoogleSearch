package com.olegka.googlesearch.room;


import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.olegka.googlesearch.domain.SearchResult;
import com.olegka.googlesearch.room.dao.SearchResultDao;

@Database(entities = {SearchResult.class}, version = 1, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {
    public abstract SearchResultDao getSearchResultDao();
}
