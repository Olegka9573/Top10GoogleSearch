package com.olegka.googlesearch.di;


import com.olegka.googlesearch.data.repository.BaseRepository;
import com.olegka.googlesearch.presentation.main.MainPresenter;

import javax.inject.Singleton;

import dagger.Component;

@Component(modules = {AppModule.class})
@Singleton
public interface AppComponent {
    void inject(BaseRepository baseRepository);
}
