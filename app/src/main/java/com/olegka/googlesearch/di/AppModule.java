package com.olegka.googlesearch.di;

import android.arch.persistence.room.Room;
import android.content.Context;

import com.google.gson.Gson;
import com.olegka.googlesearch.BuildConfig;
import com.olegka.googlesearch.data.server.API;
import com.olegka.googlesearch.data.server.Interceptor.AuthorizationInterceptor;
import com.olegka.googlesearch.data.server.RestService;
import com.olegka.googlesearch.room.AppDatabase;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class AppModule {

    private final Context context;

    public AppModule(Context context) {
        this.context = context;
    }

    @Provides
    Context provideContext() {
        return context;
    }

    @Provides
    @Singleton
    RestService provideRestService(String baseUrl, Gson gson, OkHttpClient okHttpClient) {
        API api = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
                .create(API.class);
        return new RestService(api);
    }

    @Provides
    @Singleton
    OkHttpClient provideOkHttpClient() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        return new OkHttpClient.Builder()
                .connectTimeout(20000, TimeUnit.MILLISECONDS)
                .readTimeout(30000, TimeUnit.MILLISECONDS)
                .addInterceptor(new AuthorizationInterceptor())
                .addInterceptor(interceptor)
                .build();
    }

    @Provides
    @Singleton
    Gson providesGson() {
        return new com.google.gson.GsonBuilder().create();
    }

    @Provides
    String providesBaseUrl() {
        return BuildConfig.BASE_URL;
    }

    @Provides
    @Singleton
    AppDatabase providesAppDatabase(Context context) {
        return Room.databaseBuilder(context,
                AppDatabase.class, "database").build();
    }
}
