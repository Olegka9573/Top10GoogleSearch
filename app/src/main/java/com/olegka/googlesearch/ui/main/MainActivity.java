package com.olegka.googlesearch.ui.main;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.olegka.googlesearch.R;
import com.olegka.googlesearch.domain.SearchResult;
import com.olegka.googlesearch.presentation.main.MainPresenter;
import com.olegka.googlesearch.presentation.main.MainView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends MvpAppCompatActivity implements MainView {

    @InjectPresenter
    MainPresenter mainPresenter;

    @BindView(R.id.search_request)
    EditText searchRequest;

    @BindView(R.id.recycler_results)
    RecyclerView recyclerResults;

    @BindView(R.id.progress_bar)
    View progressBar;

    @BindView(R.id.empty_results)
    TextView emptyResults;

    @BindView(R.id.search)
    ImageButton search;

    private SearchResultsAdapted searchResultsAdapted;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        initAdapter();
    }

    private void initAdapter() {
        searchResultsAdapted = new SearchResultsAdapted();
        recyclerResults.setAdapter(searchResultsAdapted);
    }

    @OnClick(R.id.search)
    void onClick() {
        mainPresenter.getSearchResults(searchRequest.getText().toString());
    }

    @Override
    public void onSearchResultsReceived(List<SearchResult> searchResults) {
        emptyResults.setVisibility(View.GONE);
        recyclerResults.setVisibility(View.VISIBLE);
        searchResultsAdapted.setItems(searchResults);
        recyclerResults.scrollToPosition(0);
    }

    @Override
    public void onEmptyResults() {
        emptyResults.setVisibility(View.VISIBLE);
        recyclerResults.setVisibility(View.INVISIBLE);
        searchResultsAdapted.setItems(null);
    }

    @Override
    public void showProgress(boolean showProgress) {
        search.setVisibility(showProgress ? View.GONE : View.VISIBLE);
        progressBar.setVisibility(showProgress ? View.VISIBLE : View.GONE);
        searchRequest.setEnabled(!showProgress);
    }

    @Override
    public void onError(Throwable throwable) {
        Toast.makeText(this, throwable.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
    }
}
