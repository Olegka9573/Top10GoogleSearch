package com.olegka.googlesearch.ui.main;


import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.TextView;

import com.olegka.googlesearch.R;
import com.olegka.googlesearch.domain.SearchResult;

import butterknife.BindView;
import butterknife.ButterKnife;

class SearchResultsViewHolder extends ViewHolder{

    @BindView(R.id.title)
    TextView title;

    @BindView(R.id.link)
    TextView link;

    @BindView(R.id.snippet)
    TextView snippet;

    SearchResultsViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    void bindDataToView(SearchResult searchResult) {
        title.setText(searchResult.getTitle());
        link.setText(searchResult.getLink());
        snippet.setText(searchResult.getSnippet());
    }
}
