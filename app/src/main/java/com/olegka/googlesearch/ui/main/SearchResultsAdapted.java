package com.olegka.googlesearch.ui.main;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.olegka.googlesearch.R;
import com.olegka.googlesearch.domain.SearchResult;

import java.util.List;

public class SearchResultsAdapted extends RecyclerView.Adapter<SearchResultsViewHolder> {

    private List<SearchResult> items;

    @Override
    public SearchResultsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_item_search_result, parent, false);
        return new SearchResultsViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(SearchResultsViewHolder holder, int position) {
        holder.bindDataToView(items.get(position));
    }

    public void setItems(List<SearchResult> items) {
        this.items = items;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return items == null ? 0 : items.size();
    }
}
