package com.olegka.googlesearch;

import android.app.Application;

import com.olegka.googlesearch.di.AppComponent;
import com.olegka.googlesearch.di.AppModule;
import com.olegka.googlesearch.di.DaggerAppComponent;

public class App extends Application {
    private static AppComponent appComponent;

    public static AppComponent getAppComponent() {
        return appComponent;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
    }
}
