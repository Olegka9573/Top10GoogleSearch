package com.olegka.googlesearch.domain;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import java.util.UUID;

import lombok.Data;

@Data
@Entity
public class SearchResult {
    @NonNull
    @PrimaryKey
    private String id;
    private String title;
    private String snippet;
    private String link;

    public SearchResult() {
        id = UUID.randomUUID().toString();
    }
}
