package com.olegka.googlesearch.data.repository;


import com.olegka.googlesearch.App;
import com.olegka.googlesearch.data.server.RestService;
import com.olegka.googlesearch.room.AppDatabase;

import javax.inject.Inject;

public abstract class BaseRepository {
    @Inject
    RestService restService;
    @Inject
    AppDatabase appDatabase;

    public BaseRepository() {
        App.getAppComponent().inject(this);
    }
}
