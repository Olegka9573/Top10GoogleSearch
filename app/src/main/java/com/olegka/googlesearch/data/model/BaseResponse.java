package com.olegka.googlesearch.data.model;


import com.google.gson.annotations.SerializedName;

public class BaseResponse<T> {
    @SerializedName("items")
    private T items;

    public T getItems() {
        return items;
    }
}
