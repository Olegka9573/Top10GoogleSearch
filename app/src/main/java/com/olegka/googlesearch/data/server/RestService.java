package com.olegka.googlesearch.data.server;


import com.olegka.googlesearch.domain.SearchResult;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Flowable;

public class RestService {
    private API api;

    public RestService(API api) {
        this.api = api;
    }

    public Flowable<List<SearchResult>> getSearchResults(String searchRequest, int resultsCount) {
        return api.getSearchResults(searchRequest, resultsCount)
                .map(searchResult ->
                        searchResult.getItems() == null ? new ArrayList<>() : searchResult.getItems());
    }
}
