package com.olegka.googlesearch.data.server.Interceptor;


import com.olegka.googlesearch.BuildConfig;

import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class AuthorizationInterceptor implements Interceptor {

    private final static String API_KEY_PARAMETER = "key";
    private final static String SEARCH_ENGINE_ID_PARAMETER = "cx";

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        HttpUrl url = request.url().newBuilder()
                .addQueryParameter(API_KEY_PARAMETER, BuildConfig.API_KEY)
                .addQueryParameter(SEARCH_ENGINE_ID_PARAMETER, BuildConfig.SEARCH_ENGINE_ID)
                .build();
        request = request.newBuilder().url(url).build();
        return chain.proceed(request);
    }
}
