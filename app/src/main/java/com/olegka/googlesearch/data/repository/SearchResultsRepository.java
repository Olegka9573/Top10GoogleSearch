package com.olegka.googlesearch.data.repository;


import com.olegka.googlesearch.domain.SearchResult;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class SearchResultsRepository extends BaseRepository {

    private final static int RESULTS_COUNT_10 = 10;

    public Flowable<List<SearchResult>> getTop10SearchResults(String searchRequest) {
        return restService.getSearchResults(searchRequest, RESULTS_COUNT_10)
                .doOnNext(this::saveResultsToDB)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    private void saveResultsToDB(List<SearchResult> searchResults) {
        if (!searchResults.isEmpty()) {
            appDatabase.getSearchResultDao().deleteAll();
            appDatabase.getSearchResultDao().insert(searchResults);
        }
    }
}
