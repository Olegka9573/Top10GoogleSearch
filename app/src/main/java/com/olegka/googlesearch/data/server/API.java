package com.olegka.googlesearch.data.server;


import com.olegka.googlesearch.data.model.BaseResponse;
import com.olegka.googlesearch.domain.SearchResult;

import java.util.List;

import io.reactivex.Flowable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface API {

    @GET("customsearch/v1")
    Flowable<BaseResponse<List<SearchResult>>> getSearchResults(@Query("q") String searchRequest,
                                                                @Query("num") int resultsCount);
}
